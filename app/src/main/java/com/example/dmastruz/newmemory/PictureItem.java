package com.example.dmastruz.newmemory;

public class PictureItem {

    private String qrCodeValue = "";
    private String path = "";

    public PictureItem() {
    }

    public String getQrCodeValue() {
        return qrCodeValue;
    }

    public void setQrCodeValue(String qrCodeValue) {
        this.qrCodeValue = qrCodeValue;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
