package com.example.dmastruz.newmemory;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

//creates view from data --> "https://www.youtube.com/watch?v=uic3TVp_j3M"
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<PictureItem> pictureItems;
    private MainActivity mainActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView pictureCard;
        public TextView qrCodeValue;
        public ImageView picture;
        public Button button;

        public MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.clickedItem = getAdapterPosition();
                    mainActivity.takeQrCodePicture();
                }
            });
            this.pictureCard = (CardView) view.findViewById(R.id.card_view);
            this.qrCodeValue = (TextView) view.findViewById(R.id.qrCodeCalue);
            this.picture = (ImageView) view.findViewById(R.id.path);
            this.button = (Button) view.findViewById(R.id.add_button);
        }
    }

    public RecyclerViewAdapter(Context mContext, List<PictureItem> pictureItems, MainActivity mainActivity) {
        this.mContext = mContext;
        this.pictureItems = pictureItems;
        this.mainActivity = mainActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == R.layout.picture_card) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_card, parent, false);
        } else if (viewType == R.layout.button_card) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.button_card, parent, false);
        }

        return new MyViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //When an item is loaded and will be displayed soon.
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        if (position == pictureItems.size()) {
            //Item = button
        } else {
            //item = PictureItem
            if (!pictureItems.get(position).getQrCodeValue().equals("") && !pictureItems.get(position).getPath().equals("")) {
                holder.qrCodeValue.setText(pictureItems.get(position).getQrCodeValue());
                holder.picture.setImageBitmap(BitmapFactory.decodeFile(pictureItems.get(position).getPath()));
            } else {
                holder.picture.setImageDrawable(mContext.getResources().getDrawable(R.drawable.thumb));
                holder.qrCodeValue.setText("+");
            }
        }

    }

    @Override
    public int getItemViewType(int position) {
        return (position == pictureItems.size()) ? R.layout.button_card : R.layout.picture_card;
    }

    @Override
    public int getItemCount() {
        return pictureItems.size() + 1;
    }

}

